# Brazilian Portuguese translation of gnarwl
# Copyright (C) 2007 Felipe Augusto van de Wiel (faw).
# This file is distributed under the same license as the gnarwl package.
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-12-02 06:20+0100\n"
"PO-Revision-Date: 2007-04-02 00:31-0300\n"
"Last-Translator: Felipe Augusto van de Wiel (faw) <faw@debian.org>\n"
"Language-Team: l10n portuguese <debian-l10n-portuguese@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"pt_BR utf-8\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Name/address of the LDAP server:"
msgstr "Nome/endereÃ§o do servidor LDAP:"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Gnarwl contacts a LDAP server in order to get information about vacation "
"messages and accounts. Please specify the server, optionally with the port "
"to be used."
msgstr ""
"Gnarwl contata o servidor LDAP para obter informaÃ§Ã£o sobre mensagens de "
"fÃ©rias e contas. Por favor, especifique o servidor, opcionalmente com a "
"porta a ser usada."

#. Type: string
#. Description
#: ../templates:1001
msgid "Example: ldap.yourdomain.local:389"
msgstr "Exemplo: ldap.seudominio.local:389"

#. Type: string
#. Description
#: ../templates:2001
msgid "Base DN of the LDAP server:"
msgstr "Base DN do seu servidor LDAP:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"In order to access the LDAP server, please specify the base gnarwl should "
"use for LDAP queries."
msgstr ""
"Para poder acessar o servidor LDAP, por favor especifique a base a ser usada "
"pelo gnarwl para as consultas LDAP."

#. Type: string
#. Description
#: ../templates:2001
msgid "Example: dc=yourdomain,dc=somewhere"
msgstr "Exemplo: dc=seudomino,dc=algumlugar"
